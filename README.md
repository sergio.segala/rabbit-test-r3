1. Setup base system

```
rabbitmqctl add_vhost print
rabbitmqctl set_permissions -p print guest ".*" ".*" ".*"
```

2. Start receiver (create exchange/queue/bind)

```
php receiver3.php
```

3. Send message (discard by client)

```
php sender3.php
```

4. Configure alternate exchange (AE)
   (https://www.mon-code.net/post/114/RabbitMQ-no-longer-loses-message-with-the-use-of-Alternate-Exchange-and-fanout)

```
rabbitmqadmin --vhost=print declare exchange name=print_exchange_ae type=fanout durable=true
rabbitmqadmin --vhost=print declare queue name=print_queue_ae durable=true
rabbitmqadmin --vhost=print declare binding source=print_exchange_ae destination=print_queue_ae
rabbitmqadmin --vhost=print declare policy name=AE pattern="^print_exchange$" apply-to=exchanges definition='{"alternate-exchange": "print_exchange_ae"}'
```

5. Moved to AE

```
php sender4.php
```

4. Configure dead letter exchange/dead letter routing key (DLX/DLK)
   (https://www.mon-code.net/post/114/RabbitMQ-no-longer-loses-message-with-the-use-of-Alternate-Exchange-and-fanout)

```
rabbitmqadmin --vhost=print declare exchange name=print_exchange_dlx type=direct durable=true
rabbitmqadmin --vhost=print declare queue name=print_queue_dlx durable=true
rabbitmqadmin --vhost=print declare binding source=print_exchange_dlx destination=print_queue_dlx
rabbitmqadmin --vhost=print declare policy name=DLX pattern="^print_queue$" apply-to=queues definition='{"dead-letter-exchange": "", "dead-letter-routing-key": "print_queue_dlx"}'
-- Retry
rabbitmqadmin --vhost=print declare policy name=Retry pattern="^print_queue_dlx$" apply-to=queues definition='{"dead-letter-exchange": "", "dead-letter-routing-key": "print_queue", "message-ttl": 15000}'


-- SS: SERVONO EXCHANGE???
```
