<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest', 'print');
$channel = $connection->channel();

$excahngeName = 'print_exchange';

// Declare exchange
$channel->exchange_declare($excahngeName, 'topic', false, true, false);

$printTypes = [
    [
        'type' => 'single',
        'priority' => 9
    ],
    [
        'type' => 'list',
        'priority' => 8
    ],
    [
        'type' => 'bulk',
        'priority' => 1
    ]
];

for ($i = 1; $i <= 10; $i++) {
    $printType = rand(0, count($printTypes) - 1);
    $text = sprintf(
        "Print type #%02d: %-6s (Pri=%d)",
        $i,
        $printTypes[$printType]['type'],
        $printTypes[$printType]['priority']
    );
    echo "{$text}\n";
    $routingKey = "print.{$printTypes[$printType]['type']}.sub-type";
    $msg = new AMQPMessage(
        $text,
        [
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
            'priority' => $printTypes[$printType]['priority']
        ]
    );
    $channel->basic_publish($msg, $excahngeName, $routingKey);
}
$channel->close();
$connection->close();
