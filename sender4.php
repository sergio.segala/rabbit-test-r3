<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest', 'print');
$channel = $connection->channel();

$excahngeName = 'print_exchange';

// Declare exchange
$channel->exchange_declare($excahngeName, 'topic', false, true, false);

echo "Send message with invalid routing key\n";
$routingKey = "invalid_routing_key";
$msg = new AMQPMessage(
    $text,
    [
        'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
        'priority' => 0
    ]
);
$channel->basic_publish($msg, $excahngeName, $routingKey);

$channel->close();
$connection->close();
