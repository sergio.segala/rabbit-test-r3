<?php

sleep(30 * 24 * 60 * 60);

// each message consumer declare the queues and bindings it needs before trying to attach to it
// IMG FAILS: https://miro.medium.com/max/1531/1*S0HNtDbsoT4-QwYgs0ohFA.png
// fatal: reject no-requeue (che manda in dead letter exchange)

// DEAD LETTER ()
//./rabbitmqadmin declare queue name="notifications" 'arguments={"x-dead-letter-exchange": "mishaps"}'
//./rabbitmqadmin declare binding source="events" destination="notifications" routing_key="notifications.v1" destination_type="queue"
// Limite coda (poi dead letter)
//./rabbitmqadmin declare queue name="notifications" 'arguments={"x-max-length": 10000}'
// TTL coda
///rabbitmqadmin declare queue name="notifications" 'arguments={"x-message-ttl": 60000}'

// !!!vedi https://medium.com/@warolv/handling-rabbitmq-consumer-failures-with-maxretry-handler-eb0332ab98e0
//https://github.com/ricbra/rabbitmq-retry-queue-example/blob/master/producer.php
