<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Wire\AMQPTable;
use PhpAmqpLib\Message\AMQPMessage;

echo "Connect to rabbitmq\n";
$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest', 'print');
$channel = $connection->channel();

$exchange           = 'my_exchange';
$queue              = 'task';
$deadLetterExchange = 'my_exchange.dead_letter';
$retryQueue         = 'retry_task';

echo "Declare exchange\n";
$channel->exchange_declare($exchange, 'direct', false, true, false);
$channel->exchange_declare($deadLetterExchange, 'direct', false, true, false);

echo "Declare queue\n";
$channel->queue_declare($queue, false, true, false, false, false, new AMQPTable([
    'x-dead-letter-exchange' => '',
    'x-dead-letter-routing-key' => $retryQueue
]));

$channel->queue_declare($retryQueue, false, true, false, false, false, new AMQPTable([
    'x-dead-letter-exchange' => '',
    'x-dead-letter-routing-key' => $queue,
    'x-message-ttl' => 5000
]));

echo "Bind queue\n";
$channel->queue_bind($queue, $exchange);
$channel->queue_bind($retryQueue, $deadLetterExchange);


$callback = function ($msg) use ($channel) {
    echo " [x] Received ", $msg->body, " on ", date('Y-m-d, H:i:s'), "\n";
    echo " [-] Cannot process crap. Nacking message. \n";
    if ($msg->has('application_headers')) {
        $headers = $msg->get('application_headers');
        $retryNr = $headers->getNativeData()['x-death'][0]['count'];
    } else {
        $retryNr = 0;
    }
    echo " [-] Retry #{$retryNr}\n";
    if ($retryNr >= 10) {
        echo " [-] REMOVED: Too many retry\n";
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    } else {
        $msg->delivery_info['channel']->basic_nack($msg->delivery_info['delivery_tag']);
    }
};


$channel->basic_qos(null, 1, null);
$channel->basic_consume($queue, '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();
